import time
import random
import matplotlib.pyplot as plt


# Преобразование вводимых значений из строковых в числовые с помощью метода integerate
def integerate(matrix):
    for i in range(len(matrix)):
        matrix[i] = int(matrix[i])
    return matrix


def calculate(matrix1, matrix2):
    if matrix1[1] == matrix2[0]:
        # a и b - наше дано. Объявление их пустыми для последующей работы
        a = []
        b = []

        # Заполнение матрицы a
        for i in range(matrix1[0]):
            if matrix1[1] == 1:
                a_el = [random.uniform(1.0, 100.0)]
            else:
                a_el = [random.uniform(1.0, 100.0)] * matrix1[1]
            a.append(a_el)

        # Заполнение матрицы b
        for i in range(matrix2[0]):
            if matrix2[1] == 1:
                b_el = [random.uniform(1.0, 100.0)]
            else:
                b_el = [random.uniform(1.0, 100.0)] * matrix1[1]
            b.append(b_el)

        # Начало вычислений
        m = len(a)
        n = len(b)
        k = len(b[0])

        # c - Искомое
        c = [[None for __ in range(k)] for __ in range(m)]  # Пустой структурированный список
        for i in range(m):  # Замена None на настоящие значения матрицы
            for j in range(k):
                c[i][j] = sum(a[i][k] * b[k][j] for k in range(n))
        return c

times = []
all_sizes = []

for i in range(1, 500, 50):
    size = i
    size1 = [size, size]
    size2 = size1
    all_sizes.append(i)
    time_start = time.perf_counter()
    result = calculate(size1, size2)
    time_end = time.perf_counter()
    times.append(time_end-time_start)


plt.plot(all_sizes, times)
plt.xlabel('Размерность матрицы')
plt.ylabel('Время вычисления, с')
plt.legend()
plt.show()
